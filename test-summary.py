#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import xlrd
import xlsxwriter

# Change the file name in line 8 if you want to check a different file
df = pd.read_excel("for_serena_full.xlsx")

# Some of the longer files have an extra column full of NaNs-- drop last column
if len(df.columns) == 4:
    df.drop(df.columns[len(df.columns)-1], axis=1, inplace=True)
df.columns = ['0','1','2']
idx = df.index[df['0'] == "# END OF TEST SUMMARY"].tolist()

count = 0 
ln = len(idx)
results = pd.DataFrame(columns = ['Part ID', 'Test result'])

for x in idx: 
    if count == ln-1:
        for i in range(idx[-1], df.tail(1).index.item()):
            if(df["0"].iloc[i].startswith("? PID ")):
                ids = df["0"].iloc[i].lstrip("? PID ")
                results = results.append({'Part ID' : ids, 'Test result' : df["2"].iloc[i]},  
                ignore_index = True) 
                results.sort_values(by=['Part ID'])                     
    else:
        for i in range(idx[count], idx[count+1]):      
            if(df["0"].iloc[i].startswith("? PID ")):
                ids = df["0"].iloc[i].lstrip("? PID ")
                results = results.append({'Part ID' : ids, 'Test result' : df["2"].iloc[i]},  
                ignore_index = True) 
                results.sort_values(by=['Part ID'])
        count = count+1    

p = (results['Test result'] == 'PASS').sum()
f = (results['Test result'] == 'FAIL').sum()
results.sort_values(by=['Part ID'], ascending=True, inplace=True)
results = results.append({'Part ID' : "Total pass: " + str(p), 'Test result' : "Total fail: " + str(f)}, ignore_index = True) 

with pd.option_context('display.max_rows', None):
    print (results)

# Change file path and file name as desired
writer = pd.ExcelWriter('output.xlsx')
results.to_excel(writer, engine='xlsxwriter')
writer.save()
